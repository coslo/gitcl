#!/usr/bin/env python

from setuptools import setup

with open('README.md', 'r') as fh:
    readme = fh.read()

args = dict(name='git_changelog',
            version='0.0.1',
            description='Minimal git changelog generator',
            long_description=readme,
            long_description_content_type="text/markdown",
            author='Daniele Coslovich',
            author_email='',
            url='https://framagit.org/coslo/gitcl',
            scripts=['gitcl'],
            install_requires=['GitPython'],
            license='GPLv3',
            classifiers=[
                'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
                'Development Status :: 5 - Production/Stable',
                'Programming Language :: Python :: 2',
                'Programming Language :: Python :: 2.7',
                'Programming Language :: Python :: 3',
                'Programming Language :: Python :: 3.6',
            ]
            )

setup(**args)
